package rscodes

import (
	"bytes"

	"github.com/klauspost/reedsolomon"
)

const (
	nFrags  = 4
	nParity = 2
)

type Fragment struct {
	Data   []byte
	Offset uint
}

type Fragmenter struct {
	enc reedsolomon.Encoder
}

func NewFragmenter() (*Fragmenter, error) {
	return NewFragmenterMK(nFrags, nParity)
}

func NewFragmenterMK(m, k int) (*Fragmenter, error) {
	enc, err := reedsolomon.New(m, k)
	if err != nil {
		return nil, err
	}
	return &Fragmenter{
		enc: enc,
	}, nil
}

func (f *Fragmenter) Split(data []byte) ([]Fragment, error) {
	shards, err := f.enc.Split(data)
	if err != nil {
		return nil, err
	}
	err = f.enc.Encode(shards)
	if err != nil {
		return nil, err
	}
	frags := make([]Fragment, len(shards))
	for i, shard := range shards {
		frag := Fragment{
			Data:   shard,
			Offset: uint(i),
		}
		frags[i] = frag
	}
	return frags, nil
}

func (f *Fragmenter) Reconstruct(frags []Fragment, outSize int) ([]byte, error) {
	shards := make([][]byte, len(frags))
	for _, frag := range frags {
		if frag.Data != nil && len(frag.Data) != 0 {
			shards[frag.Offset] = frag.Data
		}
	}
	f.enc.Reconstruct(shards)
	if _, err := f.enc.Verify(shards); err != nil {
		return nil, err
	}
	var w bytes.Buffer
	if err := f.enc.Join(&w, shards, outSize); err != nil {
		return nil, err
	}
	return w.Bytes(), nil
}
