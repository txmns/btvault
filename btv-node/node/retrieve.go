package node

import (
	"fmt"
	"io"
	"net"
	"os"
	"strconv"
	"strings"
	"time"
)

var retrieveSubcommands = map[string]func(*Node, net.Conn, []string) error{
	"REQUEST": (*Node).retrieveRequest,
}

func (n *Node) SendRetrieveRequest(recipID, fragID string) (net.Conn, error) {
	ip, err := getLocalIP()
	if err != nil {
		return nil, fmt.Errorf("getting IP address: %v", err)
	}
	port, err := getPort()
	if err != nil {
		return nil, fmt.Errorf("choosing port: %v", err)
	}
	msg := fmt.Sprintf("RETRIEVE REQUEST %s %s %d\n", fragID, ip.String(), port)
	if err := n.send(recipID, msg); err != nil {
		return nil, fmt.Errorf("failed to send the message %q: %v", msg, err)
	}

	// Wait for connection for 5 seconds.
	l, err := net.Listen("tcp", ":"+strconv.Itoa(port))
	if err != nil {
		return nil, fmt.Errorf("listening: %v", err)
	}
	defer l.Close()
	if l, ok := l.(*net.TCPListener); ok {
		if err = l.SetDeadline(time.Now().Add(acceptDeadline * time.Second)); err != nil {
			return nil, fmt.Errorf("setting a deadline: %v", err)
		}
	}
	conn, err := l.Accept()
	if err != nil {
		return nil, fmt.Errorf("accepting connection: %v", err)
	}

	return conn, nil
}

func (n *Node) retrieve(conn net.Conn, args []string) error {
	if len(args) == 0 {
		return fmt.Errorf("wrong number of arguments")
	}
	sub := strings.ToUpper(args[0])
	if f, ok := retrieveSubcommands[sub]; ok {
		if err := f(n, conn, args[1:]); err != nil {
			return fmt.Errorf("%s: %v", sub, err)
		}
		return nil
	}

	return fmt.Errorf("invalid arguments: %v", args)
}

func (n *Node) retrieveRequest(_ net.Conn, args []string) (err error) {
	if len(args) != 3 {
		return fmt.Errorf("wrong number of arguments")
	}

	// Select file in DB.
	database := n.repo.Database()
	file, err := database.SelectFileByID(args[0])
	if err != nil {
		return fmt.Errorf("selecting file: %v", err)
	}

	// Connect to the partner.
	ip := args[1]
	port := args[2]
	addr := net.JoinHostPort(ip, port)
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		return fmt.Errorf("dialing %q: %v", addr, err)
	}
	defer conn.Close()

	// Open file.
	f, err := os.Open(file.Path)
	if err != nil {
		return fmt.Errorf("opening file: %v", err)
	}
	defer f.Close()

	// Get file size.
	fi, err := f.Stat()
	if err != nil {
		return fmt.Errorf("stating file: %v", err)
	}

	// Send file size.
	fmt.Fprintf(conn, "%d\n", fi.Size())

	// Send file data.
	_, err = io.Copy(conn, f)

	return err
}
