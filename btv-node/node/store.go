package node

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/db"
	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/meta"
	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/repo"
	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/torrent"
)

const (
	acceptDeadline = 10 // in seconds.
)

var storeSubcommands = map[string]func(*Node, net.Conn, []string) error{
	"REQUEST": (*Node).storeRequest,
	"ACCEPT":  (*Node).storeAccept,
}

func (n *Node) SendStoreRequest(cont *db.Contract, set *db.BackupSet) {
	msg := fmt.Sprintf("STORE REQUEST %s %s %d\n", cont.ID, set.ID, set.Size)
	if err := n.send(cont.PartnerID, msg); err != nil {
		log.Printf("failed to send the message %q: %v\n", msg, err)
	}
}

func (n *Node) store(conn net.Conn, args []string) error {
	if len(args) == 0 {
		return fmt.Errorf("wrong number of arguments")
	}
	sub := strings.ToUpper(args[0])
	if f, ok := storeSubcommands[sub]; ok {
		if err := f(n, conn, args[1:]); err != nil {
			return fmt.Errorf("%s: %v", sub, err)
		}
		return nil
	}

	return fmt.Errorf("invalid arguments: %v", args)
}

func (n *Node) storeRequest(_ net.Conn, args []string) (err error) {
	if len(args) != 3 {
		return fmt.Errorf("wrong number of arguments")
	}

	// Select contract in DB.
	database := n.repo.Database()
	cont, err := database.SelectContractByID(args[0])
	if err != nil {
		return fmt.Errorf("selecting contract: %v", err)
	}

	// Lookup contract in repository.
	metaContID := meta.GetContractID(n.cfg, cont.ID)
	data, err := n.repo.LookupMeta(metaContID)
	if err != nil {
		return fmt.Errorf("looking up metaContID %q: %v", metaContID, err)
	}
	metaCont, err := meta.ContractFrom(n.cfg, bytes.NewBuffer(data))
	if err != nil {
		return fmt.Errorf("creating Contract: %v", err)
	}

	// Check if the storage claim is valid.
	size, err := strconv.ParseUint(args[2], 10, 64)
	if err != nil {
		return fmt.Errorf("failed to convert size %q: %v", args[2], err)
	}
	if metaCont.Me.AvailableSpace() < size {
		// Ignore.
		return nil
	}

	// Send a store accept.
	setID := args[1]
	ip, err := getLocalIP()
	if err != nil {
		return fmt.Errorf("getting IP address: %v", err)
	}
	port, err := getPort()
	if err != nil {
		return fmt.Errorf("choosing port: %v", err)
	}
	msg := fmt.Sprintf("STORE ACCEPT %s %s %s %d\n", cont.ID, setID, ip.String(), port)
	if err = n.send(cont.PartnerID, msg); err != nil {
		return fmt.Errorf("sending trade accept: %v", err)
	}

	// Wait for connection for 5 seconds.
	l, err := net.Listen("tcp", ":"+strconv.Itoa(port))
	if err != nil {
		return fmt.Errorf("listening: %v", err)
	}
	defer l.Close()
	if l, ok := l.(*net.TCPListener); ok {
		if err = l.SetDeadline(time.Now().Add(acceptDeadline * time.Second)); err != nil {
			return fmt.Errorf("setting a deadline: %v", err)
		}
	}
	conn, err := l.Accept()
	if err != nil {
		return fmt.Errorf("accepting connection: %v", err)
	}
	defer conn.Close()

	// Get torrent file size.
	tmp, err := bufio.NewReader(conn).ReadString('\n')
	size, err = strconv.ParseUint(tmp[:len(tmp)-1], 10, 64)
	if err != nil {
		return fmt.Errorf("failed to convert torrentSize %q: %v", tmp, err)
	}

	// Get Torrent.
	var buf bytes.Buffer
	_, err = io.CopyN(&buf, conn, int64(size))
	if err != nil {
		return fmt.Errorf("recieving torrent: %v", err)
	}
	t, err := torrent.Load(&buf)
	if err != nil {
		return fmt.Errorf("loading torrent meta info: %v", err)
	}

	// Store torrent in the repository.
	path, err := n.repo.StoreTorrent(t)
	if err != nil {
		return fmt.Errorf("storing torrent meta info: %v", err)
	}

	// Add torrent file to the client.
	if err = n.repo.AddTorrentFile(path); err != nil {
		return fmt.Errorf("adding torrent %q: %v", path, err)
	}

	// Add information about each fragment into Contract and DB.
	ti, err := t.UnmarshalInfo()
	if err != nil {
		return fmt.Errorf("unmarshaling torrent: %v", err)
	}
	for _, fi := range ti.Files {
		// Add into DB.
		name := fi.DisplayPath(&ti)
		file := db.File{
			ID:   name,
			Path: filepath.Join(n.repo.DataDir(), ti.Name, name),
		}
		if err = database.InsertFile(file); err != nil {
			return fmt.Errorf("inserting file: %v", err)
		}

		// Add to Contract.
		if err = metaCont.Me.Add(name, uint64(fi.Length)); err != nil {
			return fmt.Errorf("adding file to Contract: %v", err)
		}
	}

	// Store contract in the repository.
	if _, err = n.repo.StoreMeta(metaContID, []byte(metaCont.String()+"\n")); err != nil {
		return fmt.Errorf("storing Contract: %v", err)
	}

	return nil
}

func (n *Node) storeAccept(_ net.Conn, args []string) (err error) {
	if len(args) != 4 {
		return fmt.Errorf("wrong number of arguments")
	}

	database := n.repo.Database()

	// Select contract in DB.
	contID := args[0]
	cont, err := database.SelectContractByID(contID)
	if err != nil {
		return fmt.Errorf("selecting contract: %v", err)
	}

	// Lookup contract in repository.
	metaContID := meta.GetContractID(n.cfg, cont.ID)
	data, err := n.repo.LookupMeta(metaContID)
	if err != nil {
		return fmt.Errorf("looking up metaContID %q: %v", metaContID, err)
	}
	metaCont, err := meta.ContractFrom(n.cfg, bytes.NewBuffer(data))
	if err != nil {
		return fmt.Errorf("creating Contract: %v", err)
	}

	// Find BackupSet in DB.
	setID := args[1]
	set, err := database.SelectBackupSetByID(setID)
	if err != nil {
		return fmt.Errorf("selecting BackupSet: %v", err)
	}

	// Add stored information into DB.
	stored := db.Stored{BackupSetID: set.ID, ContractID: cont.ID}
	if err = database.InsertStored(stored); err != nil {
		return fmt.Errorf("inserting Stored: %v", err)
	}

	// Connect to the partner.
	ip := args[2]
	port := args[3]
	addr := net.JoinHostPort(ip, port)
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		return fmt.Errorf("dialing %q: %v", addr, err)
	}
	defer conn.Close()

	// Open torrent file.
	f, err := os.Open(set.TorrentPath)
	if err != nil {
		return fmt.Errorf("opening torrent file: %v", err)
	}
	defer f.Close()

	// Get torrent file size.
	fi, err := f.Stat()
	if err != nil {
		return fmt.Errorf("stating torrent file: %v", err)
	}

	// Send torrent file size.
	fmt.Fprintf(conn, "%d\n", fi.Size())

	// Send torrent data.
	_, err = io.Copy(conn, f)

	if _, err = f.Seek(0, 0); err != nil {
		return fmt.Errorf("resetting torrent file reader: %v", err)
	}
	t, err := torrent.Load(f)
	if err != nil {
		return fmt.Errorf("loading torrent meta info: %v", err)
	}

	// Add information about each fragment into Contract.
	ti, err := t.UnmarshalInfo()
	if err != nil {
		return fmt.Errorf("unmarshaling torrent: %v", err)
	}
	for _, fi := range ti.Files {
		name := fi.DisplayPath(&ti)

		// Add to Contract.
		if err = metaCont.Partner.Add(name, uint64(fi.Length)); err != nil {
			return fmt.Errorf("adding file to Contract: %v", err)
		}

		// Lookup HolderList meta info (locally and globally).
		// If it exists, then use it. Otherwise, create a new one.
		var hl *meta.HolderList
		if data, err := n.repo.LookupMeta(name); err != nil {
			if err != repo.ErrNotFound {
				return fmt.Errorf("looking up HolderListID %q: %v", name, err)
			}
			hl = meta.NewHolderList(n.cfg)
		} else {
			if hl, err = meta.HolderListFrom(n.cfg, bytes.NewBuffer(data)); err != nil {
				return fmt.Errorf("creating HolderList: %v", err)
			}
		}

		// Add contract partner to HolderList.
		hl.Add(cont.PartnerID)

		// Store HolderList in the repository.
		if _, err = n.repo.StoreMeta(name, []byte(hl.String()+"\n")); err != nil {
			return fmt.Errorf("storing HolderList: %v", err)
		}
	}

	// Store contract in the repository.
	if _, err = n.repo.StoreMeta(metaContID, []byte(metaCont.String()+"\n")); err != nil {
		return fmt.Errorf("storing Contract: %v", err)
	}

	return nil
}
