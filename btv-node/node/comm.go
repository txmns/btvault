package node

import (
	"bufio"
	"log"
	"net"
	"strings"
)

type Communicator struct {
	node *Node
	quit bool
}

var Commands = map[string]func(*Node, net.Conn, []string) error{
	"TRADE":    (*Node).trade,
	"STORE":    (*Node).store,
	"RETRIEVE": (*Node).retrieve,
}

func NewCommunicator(node *Node) *Communicator {
	return &Communicator{
		node: node,
		quit: false,
	}
}

func (comm *Communicator) Start() {
	addr := net.JoinHostPort("0.0.0.0", comm.node.port)
	log.Printf("starting communicator on %s\n", addr)

	l, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalln("listen error:", err)
	}
	defer l.Close()

	for {
		conn, err := l.Accept()
		if err != nil {
			log.Fatalln("accept error:", err)
		}
		if comm.quit {
			conn.Close()
			break
		}
		go comm.handleConnection(conn)
	}
}

func (comm *Communicator) Stop() {
	addr := net.JoinHostPort("127.0.0.1", comm.node.port)
	log.Printf("stopping communicator on %s\n", addr)

	comm.quit = true

	// Release socket.
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		log.Fatalln("dial error:", err)
	}
	conn.Close()
}

func (comm *Communicator) handleConnection(conn net.Conn) {
	defer conn.Close()

	msg, err := bufio.NewReader(conn).ReadString('\n')
	if err != nil {
		log.Printf("reading from connection: %v", err)
		return
	}

	log.Printf("recieved message: %q\n", msg)

	fields := strings.Split(strings.TrimSpace(msg), " ")
	if len(fields) == 0 {
		log.Printf("ivalid message: %q", msg)
		return
	}
	cmd := strings.ToUpper(fields[0])
	if f, ok := Commands[cmd]; ok {
		if err := f(comm.node, conn, fields[1:]); err != nil {
			log.Printf("%s: %v", cmd, err)
		}
		return
	}
	log.Printf("unknown command: %q", cmd)
}
