package node

import (
	"crypto/sha256"
	"encoding/binary"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"time"

	"gitlab.fel.cvut.cz/mansumax/btvault/config"
)

type Heartbeat struct {
	node *Node
	quit chan bool
}

func NewHeartbeat(node *Node) *Heartbeat {
	return &Heartbeat{
		node: node,
		quit: make(chan bool, 1),
	}
}

func (h *Heartbeat) Start() {
	log.Println("starting heartbeat")
	for {
		h.sendHeartbeatRequest()

		select {
		case <-time.Tick(15 * time.Minute):
			continue
		case <-h.quit:
			break
		}
	}
}

func (h *Heartbeat) Stop() {
	log.Println("stopping heartbeat")
	h.quit <- true
}

func (h *Heartbeat) sendHeartbeatRequest() {
	heartbeatURL, err := makeHeartbeatURL(h.node.cfg, h.node.port)
	if err != nil {
		log.Println(err)
	}
	if _, err := post(heartbeatURL); err != nil {
		log.Println(err)
	}
}

func makeHeartbeatURL(cfg *config.Config, port string) (string, error) {
	ip, err := getLocalIP()
	if err != nil {
		return "", err
	}
	hash, vals := heartbeatRequestHash(
		cfg.UserID,
		cfg.Factotum.PublicKey,
		ip.String(),
		port,
	)
	sig, err := cfg.Factotum.Sign(hash)
	if err != nil {
		return "", err
	}
	vals.Add("sigR", sig.R.String())
	vals.Add("sigS", sig.S.String())

	return (&url.URL{
		Scheme:   "http",
		Host:     cfg.ServerEndpoint,
		Path:     "/heartbeat",
		RawQuery: vals.Encode(),
	}).String(), nil
}

func post(url string) (string, error) {
	r, err := http.Post(url, "text/plain", nil)
	if err != nil {
		return "", err
	}
	b, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		return "", err
	}
	if r.StatusCode != http.StatusOK {
		return "", fmt.Errorf("server error: %s", string(b))
	}
	return string(b), nil
}

// heartbeatRequestHash generates a hash of the supplied arguments
// that, when signed, is used to prove that a heartbeat request originated
// from the user that owns the supplied private key.
func heartbeatRequestHash(userID, publicKey, ip, port string) ([]byte, url.Values) {
	const magic = "heartbeat-request"
	u := url.Values{}
	h := sha256.New()
	h.Write([]byte(magic))
	w := func(key, val string) {
		var l [4]byte
		binary.BigEndian.PutUint32(l[:], uint32(len(val)))
		h.Write(l[:])
		h.Write([]byte(val))
		u.Add(key, val)
	}
	w("id", userID)
	w("key", publicKey)
	w("ip", ip)
	w("port", port)

	return h.Sum(nil), u
}

func getLocalIP() (net.IP, error) {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return nil, err
	}
	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP, nil
			}
		}
	}
	return nil, errors.New("not connected to the Internet")
}
