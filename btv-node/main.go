package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"

	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/node"
	"gitlab.fel.cvut.cz/mansumax/btvault/config"

	"github.com/asaskevich/govalidator"
	"github.com/spf13/cobra"
)

const (
	defRPCPort = "7774"
	defBtvDir  = ".btvault"
)

// GlobalOptions holds values of global flags.
type GlobalOptions struct {
	Verbose    bool
	NoRPC      bool
	ConfigFile string
	RPCPort    string
}

var globalOptions GlobalOptions

// mainCmd represents the base command when called without any subcommands.
var mainCmd = &cobra.Command{
	Use:   "btv-node",
	Short: "A node of the BTVault system.",
	Long:  `This application represents a local node that participates in the P2P network (e.g. provides backup services). This node is usually launched as a daemon process, to manage your local node use btv-cli.`,
	Run: func(cmd *cobra.Command, args []string) {
		if err := runMain(&globalOptions, args); err != nil {
			log.Fatalln(err)
		}
	},
}

func main() {
	if err := mainCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	cobra.OnInitialize(initLog)

	pfs := mainCmd.PersistentFlags()

	// Persistent Flags.
	pfs.StringVar(&globalOptions.ConfigFile, "config", "", "configuration `file` (default $HOME/.btvault/config.yaml)")
	pfs.StringVar(&globalOptions.RPCPort, "rpc-port", defRPCPort, "port `number` to listen for incoming RPC requests. Will be ignored if --no-rpc is set")
	pfs.BoolVar(&globalOptions.NoRPC, "no-rpc", false, "do not setup an RPC server. Overwrites --rpc-port")
	pfs.BoolVarP(&globalOptions.Verbose, "verbose", "v", false, "verbose mode")
}

func runMain(opt *GlobalOptions, args []string) error {
	// Check port number.
	if !govalidator.IsPort(opt.RPCPort) && !opt.NoRPC {
		return fmt.Errorf("invalid port number %s", opt.RPCPort)
	}

	// Load user's config.
	cfg, err := loadConfig(opt.ConfigFile)
	if err != nil {
		return fmt.Errorf("loading config: %v", err)
	}

	// Init node.
	n, err := node.InitNode(cfg)
	if err != nil {
		return fmt.Errorf("initializing node: %v", err)
	}
	defer n.Terminate()

	if !opt.NoRPC {
		// Start the RPC client.
		nrs := node.NewNodeRPCServer(n, opt.RPCPort)
		go nrs.Start()
		defer nrs.Stop()
	}

	// Start the heartbeat.
	hb := node.NewHeartbeat(n)
	go hb.Start()
	defer hb.Stop()

	// Start the communicator.
	comm := node.NewCommunicator(n)
	go comm.Start()
	defer comm.Stop()

	// Wait for SIGINT to shutdown.
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	log.Printf("caught SIGINT.")
	log.Printf("shutting down.")

	return nil
}

// initLog initializes the standard logger.
func initLog() {
	log.SetFlags(0)
	log.SetPrefix("bvt-node: ")
}

// loadConfig loads config from the given configuration file.
func loadConfig(name string) (*config.Config, error) {
	if name == "" {
		return config.InitConfig(nil)
	}
	return config.FromFile(name)
}

// Verbosef printfs the given message when the verbose flag is set.
func Verbosef(format string, args ...interface{}) {
	if !globalOptions.Verbose {
		return
	}

	log.Printf(format, args...)
}
