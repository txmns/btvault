package meta

import (
	"crypto"
	"encoding/base64"
	"encoding/csv"
	"fmt"
	"io"
	"strings"

	"gitlab.fel.cvut.cz/mansumax/btvault/config"
)

//DHTBlob from ContractList (CL):
//	ID         = base64(sha256(privateKey & username & "contracts"))
//	Content    = aes(CL, kdf(privateKey))
//	Signature  = sign(sha256(Content)) & publicKey
//	Data       = Signature & Content

func GetContractListID(cfg *config.Config) string {
	h := cfg.Factotum.PrivateKeyHash(crypto.SHA256)
	h.Write([]byte(cfg.UserName))
	h.Write([]byte("contracts"))

	id := base64.RawURLEncoding.EncodeToString(h.Sum(nil))

	return id
}

type ContractList struct {
	items map[string]bool
	cfg   *config.Config
}

func NewContractList(cfg *config.Config) *ContractList {
	return &ContractList{
		items: make(map[string]bool),
		cfg:   cfg,
	}
}

func ContractListFrom(cfg *config.Config, r io.Reader) (*ContractList, error) {
	cl := NewContractList(cfg)

	rd := csv.NewReader(r)
	recs, err := rd.ReadAll()
	if err != nil {
		return nil, fmt.Errorf("reading CSV: %v", err)
	}
	if len(recs) > 1 {
		return nil, fmt.Errorf("invalid ContractList format")
	}

	for _, rec := range recs {
		for _, id := range rec {
			cl.items[id] = true
		}
	}

	return cl, nil
}

func (cl *ContractList) Add(id string) {
	cl.items[id] = true
}

func (cl *ContractList) Items() []string {
	ids := make([]string, 0, len(cl.items))
	for id := range cl.items {
		ids = append(ids, id)
	}
	return ids
}

func (cl *ContractList) String() string {
	return strings.Join(cl.Items(), ",")
}
