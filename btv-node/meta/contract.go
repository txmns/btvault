package meta

import (
	"bufio"
	"crypto"
	"encoding/base64"
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"strconv"
	"strings"
	"time"

	"gitlab.fel.cvut.cz/mansumax/btvault/config"
)

//DHTBlob from Contract (C):
//	ID         = base64(sha256(privateKey & ID(C)))
//	Content    = aes(C, kdf(privateKey))
//	Signature  = sign(sha256(Content)) & publicKey
//	Data       = Signature & Content

type Contract struct {
	Me       Side
	Partner  Side
	DateFrom time.Time
	DateTo   time.Time
	cfg      *config.Config
}

var ErrNoSpaceAvailable = errors.New("no space available")

func GetContractID(cfg *config.Config, sharedID string) string {
	h := cfg.Factotum.PrivateKeyHash(crypto.SHA256)
	h.Write([]byte(sharedID))

	id := base64.RawURLEncoding.EncodeToString(h.Sum(nil))

	return id
}

func NewContract(cfg *config.Config, partnerID string, sold, bought uint64) *Contract {
	return &Contract{
		Me:       *NewSide(cfg.UserID, sold),
		Partner:  *NewSide(partnerID, bought),
		DateFrom: time.Now(),
		DateTo:   time.Now(),
	}
}

func ContractFrom(cfg *config.Config, r io.Reader) (*Contract, error) {
	s := bufio.NewScanner(r)

	if !s.Scan() {
		return nil, fmt.Errorf("invalid Contract format: %v", s.Err())
	}
	me, err := SideFrom(strings.NewReader(s.Text()))
	if err != nil {
		return nil, fmt.Errorf("parsing Me %q: %v", s.Text(), err)
	}

	if !s.Scan() {
		return nil, fmt.Errorf("invalid Contract format: %v", s.Err())
	}
	partner, err := SideFrom(strings.NewReader(s.Text()))
	if err != nil {
		return nil, fmt.Errorf("parsing Partner %q: %v", s.Text(), err)
	}

	if !s.Scan() {
		return nil, fmt.Errorf("invalid Contract format: %v", s.Err())
	}
	from, err := time.Parse(time.RFC3339, s.Text())
	if err != nil {
		return nil, fmt.Errorf("parsing DateFrom %q: %v", s.Text(), err)
	}

	if !s.Scan() {
		return nil, fmt.Errorf("invalid Contract format: %v", s.Err())
	}
	to, err := time.Parse(time.RFC3339, s.Text())
	if err != nil {
		return nil, fmt.Errorf("parsing DateTo %q: %v", s.Text(), err)
	}

	return &Contract{
		Me:       *me,
		Partner:  *partner,
		DateFrom: from,
		DateTo:   to,
	}, nil
}

func (c *Contract) Add(userID, fragID string, fragSize uint64) error {
	switch userID {
	case c.Me.UserID:
		return c.Me.Add(fragID, fragSize)
	case c.Partner.UserID:
		return c.Partner.Add(fragID, fragSize)
	default:
		return fmt.Errorf("invalid UserID: %q", userID)
	}
}

func (c *Contract) String() string {
	return fmt.Sprintf("%s\n%s\n%s\n%s", c.Me.String(), c.Partner.String(),
		c.DateFrom.Format(time.RFC3339), c.DateTo.Format(time.RFC3339))
}

type Side struct {
	UserID      string
	TradedSpace Space
	fragIDs     []string
}

func NewSide(id string, space uint64) *Side {
	return &Side{
		UserID:      id,
		TradedSpace: *NewSpace(space),
		fragIDs:     make([]string, 0, 12),
	}
}

func SideFrom(r io.Reader) (*Side, error) {
	rd := csv.NewReader(r)
	recs, err := rd.ReadAll()
	if err != nil {
		return nil, fmt.Errorf("reading CSV: %v", err)
	}
	if len(recs) != 1 || len(recs[0]) != 3 {
		return nil, fmt.Errorf("invalid Side format: %q", recs)
	}

	space, err := SpaceFrom(strings.NewReader(recs[0][1]))
	if err != nil {
		return nil, fmt.Errorf("parsing TradedSpace %q: %v", recs[0][1], err)
	}

	return &Side{
		UserID:      recs[0][0],
		TradedSpace: *space,
		fragIDs:     strings.Split(recs[0][2], ","),
	}, nil
}

func (s *Side) AvailableSpace() uint64 {
	return s.TradedSpace.Total - s.TradedSpace.Used
}

func (s *Side) Add(fragID string, fragSize uint64) error {
	if err := s.TradedSpace.Add(fragSize); err != nil {
		return err
	}
	s.fragIDs = append(s.fragIDs, fragID)

	return nil
}

func (s *Side) String() string {
	return fmt.Sprintf("%s,%q,%q", s.UserID,
		s.TradedSpace.String(), strings.Join(s.fragIDs, ","))
}

type Space struct {
	Total uint64
	Used  uint64
}

func NewSpace(total uint64) *Space {
	return &Space{Total: total}
}

func SpaceFrom(r io.Reader) (*Space, error) {
	rd := csv.NewReader(r)
	recs, err := rd.Read()
	if err != nil {
		return nil, fmt.Errorf("reading CSV: %v", err)
	}
	if len(recs) != 2 {
		return nil, fmt.Errorf("invalid Space format: %q", recs)
	}

	total, err := strconv.ParseUint(recs[0], 10, 64)
	if err != nil {
		return nil, fmt.Errorf("parsing total size %q: %v", recs[0], err)
	}

	used, err := strconv.ParseUint(recs[1], 10, 64)
	if err != nil {
		return nil, fmt.Errorf("parsing used size %q: %v", recs[1], err)
	}

	return &Space{
		Total: total,
		Used:  used,
	}, nil
}

func (s *Space) Add(fragSize uint64) error {
	s.Used += fragSize
	if s.Used > s.Total {
		s.Used -= fragSize
		return ErrNoSpaceAvailable
	}
	return nil
}

func (s *Space) String() string {
	return fmt.Sprintf("%d,%d", s.Total, s.Used)
}
