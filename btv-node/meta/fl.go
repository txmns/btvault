package meta

import (
	"crypto"
	"encoding/base64"
	"encoding/csv"
	"fmt"
	"io"
	"sort"
	"strings"

	"gitlab.fel.cvut.cz/mansumax/btvault/config"
)

//DHTBlob from FileList (FL):
//	ID         = base64(sha256(privateKey & username & "files"))
//	Content    = aes(FL, kdf(privateKey))
//	Signature  = sign(sha256(Content)) & publicKey
//	Data       = Signature & Content

func GetFileListID(cfg *config.Config) string {
	h := cfg.Factotum.PrivateKeyHash(crypto.SHA256)
	h.Write([]byte("files"))

	id := base64.RawURLEncoding.EncodeToString(h.Sum(nil))

	return id
}

type FileList struct {
	items map[string]string
	cfg   *config.Config
}

func NewFileList(cfg *config.Config) *FileList {
	return &FileList{
		items: make(map[string]string),
		cfg:   cfg,
	}
}

func FileListFrom(cfg *config.Config, r io.Reader) (*FileList, error) {
	idx := NewFileList(cfg)

	rd := csv.NewReader(r)
	recs, err := rd.ReadAll()
	if err != nil {
		return nil, fmt.Errorf("reading CSV: %v", err)
	}
	if len(recs) > 0 && len(recs[0]) != 2 {
		return nil, fmt.Errorf("invalid FileList format")
	}

	for _, rec := range recs {
		idx.items[rec[0]] = rec[1]
	}

	return idx, nil
}

func (idx *FileList) HasID(id string) string {
	if path, ok := idx.items[id]; ok {
		return path
	}
	return ""
}

func (idx *FileList) HasPath(path string) string {
	id := GetFileBlockListID(idx.cfg, path)
	if idx.HasID(id) != "" {
		return id
	}
	return ""
}

func (idx *FileList) Add(path string) string {
	id := GetFileBlockListID(idx.cfg, path)
	idx.items[id] = path

	return id
}

func (idx *FileList) Items() ItemList {
	return sortItems(idx.items)
}

func (idx *FileList) String() string {
	strs := make([]string, 0, len(idx.items))

	for id, path := range idx.items {
		strs = append(strs, fmt.Sprintf("%s,%q", id, path))
	}

	return strings.Join(strs, "\n")
}

// A data structure to hold a key/value pair.
type Item struct {
	Key   string
	Value string
}

// A slice of Pairs that implements sort.Interface to sort by Value.
type ItemList []Item

func (il ItemList) Swap(i, j int)      { il[i], il[j] = il[j], il[i] }
func (il ItemList) Len() int           { return len(il) }
func (il ItemList) Less(i, j int) bool { return strings.Compare(il[i].Value, il[j].Value) < 0 }

// A function to turn a map into a ItemList, then sort and return it.
func sortItems(m map[string]string) ItemList {
	il := make(ItemList, 0, len(m))
	for k, v := range m {
		il = append(il, Item{k, v})
	}
	sort.Sort(il)
	return il
}
