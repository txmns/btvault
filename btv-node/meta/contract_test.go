package meta

import (
	"bytes"
	"testing"
	"time"
)

func TestSpace(t *testing.T) {
	s1 := &Space{
		Total: 12365,
		Used:  345,
	}

	_, err := SpaceFrom(bytes.NewReader([]byte(s1.String() + "\n")))
	if err != nil {
		t.Error(err)
	}
}

func TestSide(t *testing.T) {
	s1 := &Side{
		UserID: "UserID",
		TradedSpace: Space{
			Total: 12365,
			Used:  345,
		},
		fragIDs: []string{"fragID#1", "fragID#2", "fragID#3"},
	}

	_, err := SideFrom(bytes.NewReader([]byte(s1.String() + "\n")))
	if err != nil {
		t.Error(err)
	}
}

func TestContract(t *testing.T) {
	s1 := &Contract{
		Me: Side{
			UserID: "UserID#1",
			TradedSpace: Space{
				Total: 12365,
				Used:  345,
			},
			fragIDs: []string{"fragID#1", "fragID#2", "fragID#3"},
		},
		Partner: Side{
			UserID: "UserID#2",
			TradedSpace: Space{
				Total: 12365,
				Used:  345,
			},
			fragIDs: []string{"fragID#1", "fragID#2", "fragID#3"},
		},
		DateFrom: time.Now(),
		DateTo:   time.Now(),
	}

	_, err := ContractFrom(nil, bytes.NewReader([]byte(s1.String()+"\n")))
	if err != nil {
		t.Error(err)
	}
}
