package torrent

import (
	"io"
	"io/ioutil"
	"path/filepath"
	"time"

	"github.com/anacrolix/torrent/bencode"
	"github.com/anacrolix/torrent/metainfo"
)

const (
	transmissionConfig = "settings.json"
)

var (
	builtinAnnounceList = [][]string{
		{"udp://tracker.openbittorrent.com:80"},
		{"udp://tracker.publicbt.com:80"},
		{"udp://tracker.istole.it:6969"},
		{"udp://tracker.coppersurfer.tk:6969"},
		{"udp://tracker.leechers-paradise.org:6969"},
		{"udp://zer0day.ch:1337"},
		{"udp://explodie.org:6969"},
		{"udp://tracker.btzoo.eu:80/announce"},
		{"http://opensharing.org:2710/announce"},
	}
)

func NewMetaInfoFromDir(name string) (*metainfo.MetaInfo, error) {
	mi := metainfo.MetaInfo{
		AnnounceList: builtinAnnounceList,
		Comment:      "BTVault fragment",
		CreatedBy:    "btv-node",
		CreationDate: time.Now().Unix(),
	}
	info := metainfo.Info{
		PieceLength: 32 * 1024,
	}
	err := info.BuildFromFilePath(name)
	if err != nil {
		return nil, err
	}
	mi.InfoBytes, err = bencode.Marshal(info)
	if err != nil {
		return nil, err
	}

	return &mi, nil
}

func Load(r io.Reader) (*metainfo.MetaInfo, error) {
	return metainfo.Load(r)
}

func DumpTransmissionConfig(dir string) error {
	settings := `{
	"alt-speed-down": 50,
	"alt-speed-enabled": false,
	"alt-speed-time-begin": 540,
	"alt-speed-time-day": 127,
	"alt-speed-time-enabled": false,
	"alt-speed-time-end": 1020,
	"alt-speed-up": 50,
	"bind-address-ipv4": "0.0.0.0",
	"bind-address-ipv6": "::",
	"blocklist-enabled": false,
	"cache-size-mb": 4,
	"dht-enabled": true,
	"download-queue-enabled": false,
	"download-queue-size": 5,
	"encryption": 1,
	"idle-seeding-limit": 30,
	"idle-seeding-limit-enabled": false,
	"incomplete-dir-enabled": false,
	"lpd-enabled": true,
	"message-level": 2,
	"peer-congestion-algorithm": "",
	"peer-id-ttl-hours": 6,
	"peer-limit-global": 200,
	"peer-limit-per-torrent": 50,
	"peer-port": 51413,
	"peer-port-random-high": 65535,
	"peer-port-random-low": 49152,
	"peer-port-random-on-start": true,
	"peer-socket-tos": "default",
	"pex-enabled": true,
	"port-forwarding-enabled": true,
	"preallocation": 1,
	"prefetch-enabled": 1,
	"queue-stalled-enabled": true,
	"queue-stalled-minutes": 30,
	"ratio-limit": 2,
	"ratio-limit-enabled": false,
	"rename-partial-files": true,
	"rpc-authentication-required": false,
	"rpc-bind-address": "0.0.0.0",
	"rpc-enabled": true,
	"rpc-url": "/transmission/",
	"rpc-username": "",
	"rpc-whitelist": "127.0.0.1",
	"rpc-whitelist-enabled": true,
	"scrape-paused-torrents-enabled": true,
	"script-torrent-done-enabled": false,
	"script-torrent-done-filename": "",
	"seed-queue-enabled": false,
	"seed-queue-size": 10,
	"speed-limit-down": 100,
	"speed-limit-down-enabled": false,
	"speed-limit-up": 100,
	"speed-limit-up-enabled": false,
	"start-added-torrents": true,
	"trash-original-torrent-files": false,
	"umask": 18,
	"upload-slots-per-torrent": 14,
	"utp-enabled": true
}
`
	return ioutil.WriteFile(filepath.Join(dir, transmissionConfig), []byte(settings), 0664)
}
