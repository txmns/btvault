package db

import (
	"database/sql"
	"time"
)

type Contract struct {
	ID             string
	PartnerID      string
	SoldSpace      uint64
	BoughtSpace    uint64
	FromDate       time.Time
	ToDate         time.Time
	Accepted       bool
	insertionDate  time.Time
	lastUpdateDate time.Time
}

func createContractsTable(db *sql.DB) error {
	stmt := `
	CREATE TABLE IF NOT EXISTS Contracts (
		ID             TEXT NOT NULL PRIMARY KEY,
		PartnerID      TEXT NOT NULL,
		SoldSpace      INTEGER NOT NULL,
		BoughtSpace    INTEGER NOT NULL,
		FromDate       TIMESTAMP NOT NULL,
		ToDate         TIMESTAMP NOT NULL,
		Accepted       INTEGER NOT NULL,
		InsertionDate  TIMESTAMP NOT NULL,
		LastUpdateDate TIMESTAMP NOT NULL,
		FOREIGN KEY(PartnerID) REFERENCES Partners(ID)
	);
	`
	_, err := db.Exec(stmt)
	return err
}

func (s *SQLiteDB) InsertContract(contract Contract) error {
	stmt := `
	INSERT INTO Contracts (
		ID,
		PartnerID,
		SoldSpace,
		BoughtSpace,
		FromDate,
		ToDate,
		Accepted,
		InsertionDate,
		LastUpdateDate
	)
	VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);
	`
	now := time.Now()
	_, err := s.db.Exec(
		stmt,
		contract.ID,
		contract.PartnerID,
		contract.SoldSpace,
		contract.BoughtSpace,
		contract.FromDate,
		contract.ToDate,
		contract.Accepted,
		now,
		now,
	)
	return err
}

func (s *SQLiteDB) InsertContracts(contracts []Contract) error {
	for _, contract := range contracts {
		if err := s.InsertContract(contract); err != nil {
			return err
		}
	}
	return nil
}

func (s *SQLiteDB) UpdateContract(contract Contract) error {
	stmt := `
	UPDATE Contracts 
	SET SoldSpace=?,
	    BoughtSpace=?,
	    FromDate=?,
		ToDate=?,
		Accepted=?,
		LastUpdateDate=?
	WHERE ID=?;
	`
	_, err := s.db.Exec(
		stmt,
		contract.SoldSpace,
		contract.BoughtSpace,
		contract.FromDate,
		contract.ToDate,
		contract.Accepted,
		time.Now(),
		contract.ID,
	)
	return err
}

func (s *SQLiteDB) SelectContractByID(id string) (Contract, error) {
	stmt := `
	SELECT * FROM Contracts
	WHERE ID=?;
	`
	row := s.db.QueryRow(
		stmt,
		id,
	)
	return processContractsRow(row)
}

func processContractsRow(row *sql.Row) (Contract, error) {
	contract := Contract{}
	err := row.Scan(
		&contract.ID,
		&contract.PartnerID,
		&contract.SoldSpace,
		&contract.BoughtSpace,
		&contract.FromDate,
		&contract.ToDate,
		&contract.Accepted,
		&contract.insertionDate,
		&contract.lastUpdateDate,
	)
	if err != nil {
		return Contract{}, err
	}
	return contract, nil
}

func processContractsRows(rows *sql.Rows) (res []Contract, err error) {
	for rows.Next() {
		contract := Contract{}
		err := rows.Scan(
			&contract.ID,
			&contract.PartnerID,
			&contract.SoldSpace,
			&contract.BoughtSpace,
			&contract.FromDate,
			&contract.ToDate,
			&contract.Accepted,
			&contract.insertionDate,
			&contract.lastUpdateDate,
		)
		if err != nil {
			return nil, err
		}
		res = append(res, contract)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return
}
