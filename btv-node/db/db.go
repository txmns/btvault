package db

import (
	"database/sql"
	"fmt"
	"log"
	"path/filepath"

	_ "github.com/mattn/go-sqlite3"
)

const (
	dbFile = "btv-node-db.sqlite"
)

type SQLiteDB struct {
	db *sql.DB
}

func InitDB(dir string) (*SQLiteDB, error) {
	db, err := sql.Open("sqlite3", filepath.Join(dir, dbFile)+"?cache=shared&mode=rwc")
	if err != nil {
		return nil, err
	}
	if db == nil {
		return nil, fmt.Errorf("db nil")
	}

	// Use standard "database/sql" synchronization.
	db.SetMaxOpenConns(1)

	if err = createFilesTable(db); err != nil {
		return nil, err
	}
	if err = createBackupSetsTable(db); err != nil {
		return nil, err
	}
	if err = createPartnersTable(db); err != nil {
		return nil, err
	}
	if err = createContractsTable(db); err != nil {
		return nil, err
	}
	if err = createStoredTable(db); err != nil {
		return nil, err
	}

	return &SQLiteDB{db: db}, nil
}

func (s *SQLiteDB) Close() error {
	log.Println("closing database.")
	return s.db.Close()
}
