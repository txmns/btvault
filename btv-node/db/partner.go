package db

import (
	"database/sql"
	"time"
)

type Partner struct {
	ID             string
	insertionDate  time.Time
	lastUpdateDate time.Time
}

func createPartnersTable(db *sql.DB) error {
	stmt := `
	CREATE TABLE IF NOT EXISTS Partners (
		ID             TEXT NOT NULL PRIMARY KEY,
		InsertionDate  TIMESTAMP NOT NULL,
		LastUpdateDate TIMESTAMP NOT NULL
	);
	`
	_, err := db.Exec(stmt)
	return err
}

func (s *SQLiteDB) InsertPartner(partner Partner) error {
	stmt := `
	INSERT INTO Partners (
		ID,
		InsertionDate,
		LastUpdateDate
	)
	VALUES (?, ?, ?);
	`
	now := time.Now()
	_, err := s.db.Exec(
		stmt,
		partner.ID,
		now,
		now,
	)
	return err
}

func (s *SQLiteDB) InsertPartnerIfNotPresent(partner Partner) error {
	stmt := `
	INSERT INTO Partners (
		ID,
		InsertionDate,
		LastUpdateDate
	)
	SELECT ?, ?, ?
	WHERE NOT EXISTS (SELECT 1 FROM Partners WHERE ID=?);
	`
	now := time.Now()
	_, err := s.db.Exec(
		stmt,
		partner.ID,
		now,
		now,
		partner.ID,
	)
	return err
}

func (s *SQLiteDB) InsertPartners(partners []Partner) error {
	for _, partner := range partners {
		if err := s.InsertPartner(partner); err != nil {
			return err
		}
	}
	return nil
}

func (s *SQLiteDB) SelectPartnerByID(id string) (Partner, error) {
	stmt := `
	SELECT * FROM Partners
	WHERE ID=?;
	`
	row := s.db.QueryRow(
		stmt,
		id,
	)
	return processPartnersRow(row)
}

func processPartnersRow(row *sql.Row) (Partner, error) {
	partner := Partner{}
	err := row.Scan(
		&partner.ID,
		&partner.insertionDate,
		&partner.lastUpdateDate,
	)
	if err != nil {
		return Partner{}, err
	}
	return partner, nil
}
