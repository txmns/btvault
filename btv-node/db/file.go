package db

import (
	"database/sql"
	"time"
)

type File struct {
	ID             string
	Path           string
	insertionDate  time.Time
	lastUpdateDate time.Time
}

func createFilesTable(db *sql.DB) error {
	stmt := `
	CREATE TABLE IF NOT EXISTS Files (
		ID             TEXT NOT NULL PRIMARY KEY,
		Path           TEXT NOT NULL,
		InsertionDate  TIMESTAMP NOT NULL,
		LastUpdateDate TIMESTAMP NOT NULL
	);
	`
	_, err := db.Exec(stmt)
	return err
}

func (s *SQLiteDB) InsertFile(file File) error {
	stmt := `
	INSERT INTO Files (
		ID,
		Path,
		InsertionDate,
		LastUpdateDate
	)
	VALUES (?, ?, ?, ?);
	`
	now := time.Now()
	_, err := s.db.Exec(
		stmt,
		file.ID,
		file.Path,
		now,
		now,
	)
	return err
}

func (s *SQLiteDB) InsertFiles(files []File) error {
	for _, file := range files {
		if err := s.InsertFile(file); err != nil {
			return err
		}
	}
	return nil
}

func (s *SQLiteDB) SelectFileByID(id string) (File, error) {
	stmt := `
	SELECT * FROM Files
	WHERE ID=?;
	`
	row := s.db.QueryRow(stmt, id)
	return processFilesRow(row)
}

func processFilesRows(rows *sql.Rows) (res []File, err error) {
	for rows.Next() {
		f := File{}
		err := rows.Scan(
			&f.ID,
			&f.Path,
			&f.insertionDate,
			&f.lastUpdateDate,
		)
		if err != nil {
			return nil, err
		}
		res = append(res, f)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return
}

func processFilesRow(row *sql.Row) (File, error) {
	f := File{}
	err := row.Scan(
		&f.ID,
		&f.Path,
		&f.insertionDate,
		&f.lastUpdateDate,
	)
	if err != nil {
		return File{}, err
	}
	return f, nil
}
