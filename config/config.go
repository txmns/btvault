package config

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"

	"gitlab.fel.cvut.cz/mansumax/btvault/factotum"

	homedir "github.com/mitchellh/go-homedir"
	yaml "gopkg.in/yaml.v2"
)

// Config key words.
const (
	server          = "server"
	tracker         = "tracker"
	dht             = "dht"
	node            = "node"
	keyDir          = "keydir"
	repository      = "repository"
	userName        = "username"
	userID          = "userid"
	userCert        = "usercert"
	serverPublicKey = "serverkey"
)

const (
	DefBTVaultDir = ".btvault"
	DefConfigFile = "config.yaml"
)

// Config represent user's configuration.
type Config struct {
	UserName        string
	UserID          string
	UserCert        string
	ServerEndpoint  string
	TrackerEndpoint string
	DHTEndpoint     string
	NodeEndpoint    string
	Repository      string
	KeyDir          string
	ServerPublicKey string
	Factotum        *factotum.Factotum
}

// New returns empty configuration.
func New() *Config {
	return &Config{}
}

// FromFile initializes the user's config using the given file.
// If the file cannot be opened but the name can be found in
// $HOME/.btvault, that file is used.
func FromFile(name string) (*Config, error) {
	f, err := os.Open(name)
	if err != nil && !filepath.IsAbs(name) && os.IsNotExist(err) {
		// It's a local name, so, try adding $HOME/.btvault
		home, errHome := homedir.Dir()
		if errHome == nil {
			f, err = os.Open(filepath.Join(home, DefBTVaultDir, name))
		}
	}
	if err != nil {
		return nil, err
	}
	defer f.Close()

	return InitConfig(f)
}

// InitConfig returns a config generated from a configuration file.
// The default configuration file location is $HOME/.btvault/config.yaml.
// If passed a non-nil io.Reader, that is used instead of the default file.
func InitConfig(r io.Reader) (*Config, error) {
	if r == nil {
		home, err := homedir.Dir()
		if err != nil {
			return nil, err
		}
		f, err := os.Open(filepath.Join(home, DefBTVaultDir, DefConfigFile))
		if err != nil {
			return nil, err
		}
		defer f.Close()

		r = f
	}

	// Load config file.
	data, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}
	m, err := parseYAML(data)
	if err != nil {
		return nil, fmt.Errorf("parsing config file: %v", err)
	}

	// Create factotum.
	fact, err := factotum.NewFromDir(m[keyDir])
	if err != nil {
		return nil, err
	}

	return &Config{
		UserName:        m[userName],
		UserID:          m[userID],
		UserCert:        m[userCert],
		ServerEndpoint:  m[server],
		TrackerEndpoint: m[tracker],
		DHTEndpoint:     m[dht],
		NodeEndpoint:    m[node],
		Repository:      m[repository],
		KeyDir:          m[keyDir],
		ServerPublicKey: m[serverPublicKey],
		Factotum:        fact,
	}, nil
}

// String returns string representation of the config.
func (cfg *Config) String() string {
	m := map[string]string{
		server:          cfg.ServerEndpoint,
		tracker:         cfg.TrackerEndpoint,
		dht:             cfg.DHTEndpoint,
		node:            cfg.NodeEndpoint,
		repository:      cfg.Repository,
		keyDir:          cfg.KeyDir,
		userName:        cfg.UserName,
		userID:          cfg.UserID,
		userCert:        cfg.UserCert,
		serverPublicKey: cfg.ServerPublicKey,
	}

	// Marshal YAML.
	data, _ := yaml.Marshal(m)

	return string(data)
}

// parseYAML parses YAML from the given map and puts the values
// into the provided map. Unrecognized keys generate an error.
func parseYAML(data []byte) (map[string]string, error) {
	// Known keys.
	known := map[string]bool{
		server:          true,
		tracker:         true,
		dht:             true,
		node:            true,
		repository:      true,
		keyDir:          true,
		userName:        true,
		userID:          true,
		userCert:        true,
		serverPublicKey: true,
	}

	// Unmarshal YAML.
	m := make(map[string]string, len(known))
	if err := yaml.Unmarshal(data, m); err != nil {
		return nil, err
	}

	// Check values.
	if len(m) < len(known) {
		for k, _ := range known {
			if _, ok := m[k]; !ok {
				return nil, fmt.Errorf("value for key %q is missing", k)
			}
		}
		// Won't happen!
		return nil, fmt.Errorf("invalid config file")
	}
	for k, v := range m {
		if _, ok := known[k]; !ok {
			return nil, fmt.Errorf("unrecognized key %q", k)
		}
		if v == "" {
			return nil, fmt.Errorf("value for key %q is missing", k)
		}
	}

	return m, nil
}
