#!/bin/sh
# Does PNG exports for all SVG images in the current directory.
for svg in *.svg
do
	if [ ! -f ${svg%.*}.png ]; then
		inkscape -e "${svg%.*}.png" -D -d 300 $svg
	fi
done
