package dht

import (
	"errors"
	"fmt"
	"io"
	"net"
	"os"
	"path/filepath"
	"strconv"
)

func (dht *DHT) put(conn net.Conn, args []string) error {
	if len(args) != 2 {
		return errors.New("wrong number of arguments")
	}
	id := args[0]
	size, err := strconv.ParseInt(args[1], 10, 64)
	if err != nil {
		fmt.Fprintf(conn, "KO %q\n", fmt.Errorf("parsing size %s: %v", args[1], err))
		return nil
	}

	// Create the requested file.
	path := filepath.Join(dht.dataDir, id)
	f, err := os.Create(path)
	if err != nil {
		fmt.Fprintf(conn, "KO %q\n", err)
		return err
	}
	defer f.Close()

	// Send confirmation.
	fmt.Fprintf(conn, "OK\n", err)

	// Recieve data.
	_, err = io.CopyN(f, conn, size)

	return err
}
