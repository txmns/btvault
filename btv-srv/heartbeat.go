package main

import (
	"crypto/sha256"
	"database/sql"
	"encoding/binary"
	"fmt"
	"math/big"
	"net/http"
	"net/http/httputil"
	"net/url"

	"gitlab.fel.cvut.cz/mansumax/btvault/btv-srv/db"
	"gitlab.fel.cvut.cz/mansumax/btvault/factotum"

	"github.com/asaskevich/govalidator"
)

// heartbeatHandler implements an http.Handler that handles heartbeat requests.
type heartbeatHandler struct {
	fact *factotum.Factotum
	db   *db.SQLiteDB
}

// newHeartbeatHandler creates a new handler that serves /heartbeat.
func newHeartbeatHandler(fact *factotum.Factotum, db *db.SQLiteDB) *heartbeatHandler {
	return &heartbeatHandler{
		fact: fact,
		db:   db,
	}
}

func (h *heartbeatHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	errorf := func(code int, format string, args ...interface{}) {
		http.Error(w, fmt.Sprintf(format, args...), code)
	}

	// Save a copy of this request for debugging.
	dump, err := httputil.DumpRequest(r, true)
	if err != nil {
		errorf(http.StatusInternalServerError, "dumping request: %v", err)
		return
	}
	Verbosef(string(dump))

	// Parse and validate request.
	if r.Method != "POST" {
		errorf(http.StatusMethodNotAllowed, "method not allowed")
		return
	}
	userID := r.FormValue("id")
	publicKey := r.FormValue("key")
	ip := r.FormValue("ip")
	port := r.FormValue("port")
	sigR := r.FormValue("sigR")
	sigS := r.FormValue("sigS")
	if !govalidator.IsIP(ip) {
		errorf(http.StatusBadRequest, "invalid ip address: %s", ip)
		return
	}
	if !govalidator.IsPort(port) {
		errorf(http.StatusBadRequest, "invalid port number: %s", port)
		return
	}
	if err := verifyHeartbeatSignature(userID, publicKey, ip, port, sigR, sigS); err != nil {
		errorf(http.StatusBadRequest, "invalid request: %s", err)
		return
	}

	// Find the user in DB.
	user, err := h.db.SelectUserByUserID(userID)
	if err != nil {
		if err == sql.ErrNoRows {
			errorf(http.StatusBadRequest, "user with id=%q does not exist", userID)
		} else {
			errorf(http.StatusInternalServerError, "db error: %v", err)
		}
		return
	}

	// Update user info.
	user.IP = ip
	user.Port = port
	if err = h.db.UpdateUser(user); err != nil {
		errorf(http.StatusInternalServerError, "db error: %v", err)
		return
	}

	fmt.Fprintln(w, "OK")
}

// verifyHeartbeatSignature verifies that the heartbeat request was signed by
// the user using the private key that corresponds to the public key provided.
func verifyHeartbeatSignature(userID, publicKey, ip, port, sigR, sigS string) error {
	var rs, ss big.Int
	if _, ok := rs.SetString(sigR, 10); !ok {
		return fmt.Errorf("invalid signature R value")
	}
	if _, ok := ss.SetString(sigS, 10); !ok {
		return fmt.Errorf("invalid signature S value")
	}
	sig := factotum.Signature{R: &rs, S: &ss}
	hash, _ := heartbeatRequestHash(userID, publicKey, ip, port)

	return factotum.Verify(hash, sig, publicKey)
}

// heartbeatRequestHash generates a hash of the supplied arguments
// that, when signed, is used to prove that a heartbeat request originated
// from the user that owns the supplied private key.
func heartbeatRequestHash(userID, publicKey, ip, port string) ([]byte, url.Values) {
	const magic = "heartbeat-request"
	u := url.Values{}
	h := sha256.New()
	h.Write([]byte(magic))
	w := func(key, val string) {
		var l [4]byte
		binary.BigEndian.PutUint32(l[:], uint32(len(val)))
		h.Write(l[:])
		h.Write([]byte(val))
		u.Add(key, val)
	}
	w("id", userID)
	w("key", publicKey)
	w("ip", ip)
	w("port", port)

	return h.Sum(nil), u
}
