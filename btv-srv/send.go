package main

import (
	"crypto/sha256"
	"database/sql"
	"encoding/binary"
	"fmt"
	"io"
	"math/big"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"

	"gitlab.fel.cvut.cz/mansumax/btvault/btv-srv/db"
	"gitlab.fel.cvut.cz/mansumax/btvault/factotum"
)

// sendHandler implements an http.Handler that handles send requests.
type sendHandler struct {
	fact *factotum.Factotum
	db   *db.SQLiteDB
}

// newSendHandler creates a new handler that serves /send.
func newSendHandler(fact *factotum.Factotum, db *db.SQLiteDB) *sendHandler {
	return &sendHandler{
		fact: fact,
		db:   db,
	}
}

func (h *sendHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	errorf := func(code int, format string, args ...interface{}) {
		http.Error(w, fmt.Sprintf(format, args...), code)
	}

	// Save a copy of this request for debugging.
	dump, err := httputil.DumpRequest(r, true)
	if err != nil {
		errorf(http.StatusInternalServerError, "dumping request: %v", err)
		return
	}
	Verbosef(string(dump))

	// Parse and validate request.
	if r.Method != "POST" {
		errorf(http.StatusMethodNotAllowed, "method not allowed")
		return
	}
	senderID := r.FormValue("sender")
	publicKey := r.FormValue("key")
	recipID := r.FormValue("recip")
	msg := r.FormValue("msg")
	sigR := r.FormValue("sigR")
	sigS := r.FormValue("sigS")
	if err := verifySendSignature(senderID, publicKey, recipID, msg, sigR, sigS); err != nil {
		errorf(http.StatusBadRequest, "invalid request: %s", err)
		return
	}

	// Find the recipient in DB.
	recip, err := h.db.SelectUserByUserID(recipID)
	if err != nil {
		if err == sql.ErrNoRows {
			errorf(http.StatusBadRequest, "user with id=%q does not exist", recipID)
		} else {
			errorf(http.StatusInternalServerError, "db error: %v", err)
		}
		return
	}

	// Send the message.
	if err = sendMessage(recip, msg); err != nil {
		errorf(http.StatusInternalServerError, "failed to send the message: %v", err)
		return
	}

	fmt.Fprintln(w, "OK")
}

func sendMessage(recip db.User, msg string) error {
	addr := net.JoinHostPort(recip.IP, recip.Port)
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		return err
	}

	if msg[len(msg)-1] != '\n' {
		msg += "\n"
	}
	_, err = io.Copy(conn, strings.NewReader(msg))

	return err
}

// verifySendSignature verifies that the send request was signed by
// the user using the private key that corresponds to the public key provided.
func verifySendSignature(senderID, publicKey, recipID, msg, sigR, sigS string) error {
	var rs, ss big.Int
	if _, ok := rs.SetString(sigR, 10); !ok {
		return fmt.Errorf("invalid signature R value")
	}
	if _, ok := ss.SetString(sigS, 10); !ok {
		return fmt.Errorf("invalid signature S value")
	}
	sig := factotum.Signature{R: &rs, S: &ss}
	hash, _ := sendRequestHash(senderID, publicKey, recipID, msg)

	return factotum.Verify(hash, sig, publicKey)
}

// sendRequestHash generates a hash of the supplied arguments
// that, when signed, is used to prove that a send request originated
// from the user that owns the supplied private key.
func sendRequestHash(senderID, publicKey, recipID, msg string) ([]byte, url.Values) {
	const magic = "send-request"
	u := url.Values{}
	h := sha256.New()
	h.Write([]byte(magic))
	w := func(key, val string) {
		var l [4]byte
		binary.BigEndian.PutUint32(l[:], uint32(len(val)))
		h.Write(l[:])
		h.Write([]byte(val))
		u.Add(key, val)
	}
	w("sender", senderID)
	w("key", publicKey)
	w("recip", recipID)
	w("msg", msg)

	return h.Sum(nil), u
}
