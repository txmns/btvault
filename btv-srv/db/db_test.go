package db

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"testing"
)

func TestInitSQLiteDB(t *testing.T) {
	dir, err := ioutil.TempDir(os.TempDir(), "TestInitSQLiteDB")
	if err != nil {
		t.Error(err)
	}
	defer os.RemoveAll(dir) // clean up

	_, err = InitDB(dir)
	if err != nil {
		t.Error(err)
	}
}

func TestUsers(t *testing.T) {
	dir, err := ioutil.TempDir(os.TempDir(), "TestUsers")
	if err != nil {
		t.Error(err)
	}
	defer os.RemoveAll(dir) // clean up

	s, err := InitDB(dir)
	if err != nil {
		t.Error(err)
	}

	users, err := genRandomUsers(30, 8888)
	if err = s.InsertUsers(users); err != nil {
		t.Error(err)
	}

	_, err = s.SelectUserByUserID(users[0].UserID)
	if err != nil {
		t.Error("SelectUserByUserID failed: valid UserID")
	}

	_, err = s.SelectUserByUserID("123")
	if err == nil {
		t.Error("SelectUserByUserID failed: invalid UserID")
	}
}

func genRandomUsers(n int, seed int64) (users []User, err error) {
	rand.Seed(seed)

	for i := 0; i < n; i++ {
		id := make([]byte, 20)
		if err = genEntropy(id); err != nil {
			return nil, err
		}
		name := fmt.Sprintf("user%d@test.com", i)
		pub := make([]byte, 16)
		if err = genEntropy(pub); err != nil {
			return nil, err
		}
		cert := make([]byte, 32)
		if err = genEntropy(cert); err != nil {
			return nil, err
		}
		ip := "10.0.0.7"
		port := fmt.Sprintf("%d", 2000+i)
		reputation := rand.Float64()

		user := User{
			UserID:      base64.RawURLEncoding.EncodeToString(id),
			UserName:    name,
			PublicKey:   base64.RawURLEncoding.EncodeToString(pub),
			Certificate: base64.RawURLEncoding.EncodeToString(cert),
			IP:          ip,
			Reputation:  reputation,
			Port:        port,
		}

		users = append(users, user)
	}
	return
}

func genEntropy(entropy []byte) error {
	_, err := rand.Read(entropy)
	if err != nil {
		return err
	}
	return nil
}
