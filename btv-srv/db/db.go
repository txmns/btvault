package db

import (
	"database/sql"
	"fmt"
	"path/filepath"

	_ "github.com/mattn/go-sqlite3"
)

const (
	dbFile = "btv-srv-db.sqlite"
)

type SQLiteDB struct {
	db *sql.DB
}

func InitDB(dir string) (*SQLiteDB, error) {
	db, err := sql.Open("sqlite3", filepath.Join(dir, dbFile)+"?cache=shared&mode=rwc")
	if err != nil {
		return nil, err
	}
	if db == nil {
		return nil, fmt.Errorf("db nil")
	}

	// Use standard "database/sql" synchronization.
	db.SetMaxOpenConns(1)

	if err = createUsersTable(db); err != nil {
		return nil, err
	}

	return &SQLiteDB{db: db}, nil
}

func (s *SQLiteDB) Close() error {
	return s.db.Close()
}
