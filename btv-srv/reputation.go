package main

import (
	"crypto/sha256"
	"database/sql"
	"encoding/binary"
	"fmt"
	"io"
	"math/big"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strconv"

	"gitlab.fel.cvut.cz/mansumax/btvault/btv-srv/db"
	"gitlab.fel.cvut.cz/mansumax/btvault/factotum"
)

// reputationHandler implements an http.Handler that handles send requests.
type reputationHandler struct {
	fact *factotum.Factotum
	db   *db.SQLiteDB
}

// newReputationHandler creates a new handler that serves /reputation.
func newReputationHandler(fact *factotum.Factotum, db *db.SQLiteDB) *reputationHandler {
	return &reputationHandler{
		fact: fact,
		db:   db,
	}
}

func (h *reputationHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	errorf := func(code int, format string, args ...interface{}) {
		http.Error(w, fmt.Sprintf(format, args...), code)
	}

	// Save a copy of this request for debugging.
	dump, err := httputil.DumpRequest(r, true)
	if err != nil {
		errorf(http.StatusInternalServerError, "dumping request: %v", err)
		return
	}
	Verbosef(string(dump))

	// Parse and validate request.
	if r.Method != "GET" {
		errorf(http.StatusMethodNotAllowed, "method not allowed")
		return
	}
	senderID := r.FormValue("sender")
	publicKey := r.FormValue("key")
	userID := r.FormValue("user")
	sigR := r.FormValue("sigR")
	sigS := r.FormValue("sigS")
	if err := verifyReputationSignature(senderID, publicKey, userID, sigR, sigS); err != nil {
		errorf(http.StatusBadRequest, "invalid request: %s", err)
		return
	}

	// Find the user in DB.
	user, err := h.db.SelectUserByUserID(userID)
	if err != nil {
		if err == sql.ErrNoRows {
			errorf(http.StatusBadRequest, "user with id=%q does not exist", userID)
		} else {
			errorf(http.StatusInternalServerError, "db error: %v", err)
		}
		return
	}

	// Send response.
	io.WriteString(w, strconv.FormatFloat(user.Reputation, 'E', -1, 64))
}

// verifyReputationSignature verifies that the reputation request was signed by
// the user using the private key that corresponds to the public key provided.
func verifyReputationSignature(senderID, publicKey, userID, sigR, sigS string) error {
	var rs, ss big.Int
	if _, ok := rs.SetString(sigR, 10); !ok {
		return fmt.Errorf("invalid signature R value")
	}
	if _, ok := ss.SetString(sigS, 10); !ok {
		return fmt.Errorf("invalid signature S value")
	}
	sig := factotum.Signature{R: &rs, S: &ss}
	hash, _ := reputationRequestHash(senderID, publicKey, userID)

	return factotum.Verify(hash, sig, publicKey)
}

// reputationRequestHash generates a hash of the supplied arguments
// that, when signed, is used to prove that a send request originated
// from the user that owns the supplied private key.
func reputationRequestHash(senderID, publicKey, userID string) ([]byte, url.Values) {
	const magic = "reputation-request"
	u := url.Values{}
	h := sha256.New()
	h.Write([]byte(magic))
	w := func(key, val string) {
		var l [4]byte
		binary.BigEndian.PutUint32(l[:], uint32(len(val)))
		h.Write(l[:])
		h.Write([]byte(val))
		u.Add(key, val)
	}
	w("sender", senderID)
	w("key", publicKey)
	w("user", userID)

	return h.Sum(nil), u
}
