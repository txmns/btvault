package main

import "gitlab.fel.cvut.cz/mansumax/btvault/btv-cli/cmd"

func main() {
	cmd.Execute()
}
