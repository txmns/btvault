package cmd

import (
	"log"
	"time"

	"gitlab.fel.cvut.cz/mansumax/btvault/config"

	"github.com/briandowns/spinner"
	"github.com/spf13/cobra"
)

// GlobalOptions holds values of global flags.
type GlobalOptions struct {
	ConfigFile string
}

var globalOptions GlobalOptions
var spin *spinner.Spinner = spinner.New(spinner.CharSets[9], 100*time.Millisecond)

// RootCmd represents the base command when called without any subcommands.
var RootCmd = &cobra.Command{
	Use:   "btv-cli",
	Short: "A remote control command-line utility for btv-node.",
	Long:  `This application allows user to join the P2P network (e.g. signup) and to manage the user's locally running nodes.`,
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		log.Fatalln(err)
	}
}

func init() {
	cobra.OnInitialize(initLog)

	pfs := RootCmd.PersistentFlags()

	// Persistent Flags.
	pfs.StringVar(&globalOptions.ConfigFile, "config", "", "configuration `file` (default $HOME/.btvault/config.yaml)")
}

// initLog initializes the standard logger.
func initLog() {
	log.SetFlags(0)
	log.SetPrefix("bvt-cli: ")
}

// loadConfig loads config from the given configuration file.
func loadConfig(name string) (*config.Config, error) {
	if name == "" {
		return config.InitConfig(nil)
	}
	return config.FromFile(name)
}
