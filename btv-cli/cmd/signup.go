package cmd

import (
	"bufio"
	"crypto/sha256"
	"encoding/binary"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"

	"gitlab.fel.cvut.cz/mansumax/btvault/config"
	"gitlab.fel.cvut.cz/mansumax/btvault/factotum"
	"gitlab.fel.cvut.cz/mansumax/btvault/utils"

	"github.com/asaskevich/govalidator"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
)

const (
	defServerEndpoint  = "127.0.0.1:7771"
	defTrackerEndpoint = "127.0.0.1:7772"
	defDhtEndpoint     = "127.0.0.1:7773"
	defNodeEndpoint    = "127.0.0.1:7774"
	defRepository      = ".btvault/repo"
)

// SignupOptions holds values for signup flags.
type SignupOptions struct {
	ServerEndpoint  string
	TrackerEndpoint string
	DHTEndpoint     string
	NodeEndpoint    string
	Repository      string
	KeyDir          string
	CurveName       string
	Force           bool
}

var signupOptions SignupOptions

// signupCmd represents the signup command
var signupCmd = &cobra.Command{
	Use:   "signup [<username>]",
	Short: "Register a new user in the BTVault network",
	Long:  `The "signup" command generates an BTVault configuration file and private/public key pair, stores them locally, and sends a signup request to the public BTVault server. Username should be a valid email address in format <user name>@<domain name>.`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) > 1 {
			cmd.Usage()
			os.Exit(2)
		}
		if err := runSignup(&signupOptions, &globalOptions, args); err != nil {
			log.Fatalf("signup: %v\n", err)
		}
	},
}

// init initializes the signup command.
func init() {
	RootCmd.AddCommand(signupCmd)

	fs := signupCmd.Flags()

	fs.StringVar(&signupOptions.ServerEndpoint, "server", defServerEndpoint, "server's `endpoint` in form <host>:<port>")
	fs.StringVar(&signupOptions.TrackerEndpoint, "tracker", defTrackerEndpoint, "tracker's `endpoint` in form <host>:<port>")
	fs.StringVar(&signupOptions.DHTEndpoint, "dht", defDhtEndpoint, "dht's `endpoint` in form <host>:<port>")
	fs.StringVar(&signupOptions.NodeEndpoint, "node", defNodeEndpoint, "node's `endpoint` in form <host>:<port>")
	fs.StringVar(&signupOptions.KeyDir, "keydir", "", "`directory` to store keys (default is $HOME/.ssh)")
	fs.StringVar(&signupOptions.Repository, "repo", "", "repository `directory` (default is $HOME/.btvault/repo)")
	fs.StringVar(&signupOptions.CurveName, "curve", defCurveName, "cryptographic curve `name`: p256, p384 or p521")
	fs.BoolVarP(&signupOptions.Force, "force", "f", false, "create a new user even if keys, config file and credentials file exist")
}

// runSignup is a main function for the signup command.
func runSignup(opt *SignupOptions, gopt *GlobalOptions, args []string) error {
	// Create new config.
	cfg := config.New()

	// If no username is provided, get it directly from user.
	if len(args) > 0 && args[0] != "" {
		cfg.UserName = args[0]
	} else {
		cfg.UserName = getUserInput("Username: ")
	}

	// Check the username.
	cfg.UserName = strings.TrimSpace(cfg.UserName)
	if !govalidator.IsEmail(cfg.UserName) {
		return fmt.Errorf("invalid username %q", cfg.UserName)
	}

	// Check server's endpoint.
	cfg.ServerEndpoint = strings.TrimSpace(opt.ServerEndpoint)
	if !isValidEndpoint(cfg.ServerEndpoint) {
		return fmt.Errorf("invalid server's endpoint %q", cfg.ServerEndpoint)
	}

	// Check tracker's endpoint.
	cfg.TrackerEndpoint = strings.TrimSpace(opt.TrackerEndpoint)
	if !isValidEndpoint(cfg.TrackerEndpoint) {
		return fmt.Errorf("invalid tracker's endpoint %q", cfg.TrackerEndpoint)
	}

	// Check dht's endpoints.
	cfg.DHTEndpoint = strings.TrimSpace(opt.DHTEndpoint)
	if !isValidEndpoint(cfg.DHTEndpoint) {
		return fmt.Errorf("invalid dht's endpoint %q", cfg.DHTEndpoint)
	}

	// Check node's endpoints.
	cfg.NodeEndpoint = strings.TrimSpace(opt.NodeEndpoint)
	if !isValidEndpoint(cfg.NodeEndpoint) {
		return fmt.Errorf("invalid node's endpoint %q", cfg.NodeEndpoint)
	}

	// Check repository directory
	cfg.Repository = strings.TrimSpace(opt.Repository)
	if cfg.Repository == "" {
		home, err := homedir.Dir()
		if err != nil {
			return err
		}
		cfg.Repository = filepath.Join(home, defRepository)
	}
	absRepository, err := utils.AbsPathify(cfg.Repository)
	if err != nil {
		return fmt.Errorf("%s: %v", cfg.Repository, err)
	}
	cfg.Repository = absRepository

	// Generate a new key pair.
	kopt := KeygenOptions{
		CurveName: opt.CurveName,
		KeyDir:    opt.KeyDir,
		Force:     opt.Force,
	}
	if err = runKeygen(&kopt, gopt, nil); err != nil {
		return fmt.Errorf("genereting keys: %v", err)
	}

	// Create factotum from newly generated keys.
	// Note the usage of kopt.KeyDir.
	cfg.KeyDir = kopt.KeyDir
	fact, err := factotum.NewFromDir(cfg.KeyDir)
	if err != nil {
		return fmt.Errorf("creating factotum: %v", err)
	}
	cfg.Factotum = fact

	// Register user on the server.
	if err := registerUser(opt, cfg); err != nil {
		return fmt.Errorf("registering user: %v", err)
	}

	// Write the new config.
	if err := writeConfigFile(opt, gopt, cfg); err != nil {
		return fmt.Errorf("writing config: %v", err)
	}

	return nil
}

// isValidEndpoint checks if the given endpoint is valid
// (e.g. is in format <host>:<port>).
func isValidEndpoint(endpoint string) bool {
	fields := strings.Split(endpoint, ":")
	return len(fields) == 2 &&
		(govalidator.IsIPv4(fields[0]) && govalidator.IsPort(fields[1]))
}

// registerUser registers a new user on the server.
func registerUser(opt *SignupOptions, cfg *config.Config) error {
	// Send signup request.
	signupURL, err := makeSignupURL(opt, cfg)
	if err != nil {
		return err
	}
	if _, err := post(signupURL); err != nil {
		return err
	}

	fmt.Printf("A signup email with confirmation code has been sent to %q,\n", cfg.UserName)
	fmt.Println("In order to complete the signup process, please, enter the confirmation code from that email.")
	code := strings.TrimSpace(getUserInput("Confirmation code (... or just press <ENTER>): "))

	// Send confirmation request.
	confirmURL, err := makeConfirmURL(opt, cfg, code)
	if err != nil {
		return err
	}
	b, err := post(confirmURL)
	if err != nil {
		return err
	}

	// Parse server's response.
	fields := strings.Split(b, "&")
	// Sanity checks.
	if len(fields) != 3 {
		return fmt.Errorf("invalid server's response: %s", b)
	}
	cfg.UserID = strings.TrimSpace(fields[0])
	cfg.UserCert = strings.TrimSpace(fields[1])
	cfg.ServerPublicKey = strings.TrimSpace(fields[2])

	//TODO verify UserID and UserCert

	return nil
}

func post(url string) (string, error) {
	r, err := http.Post(url, "text/plain", nil)
	if err != nil {
		return "", err
	}
	b, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		return "", err
	}
	if r.StatusCode != http.StatusOK {
		return "", fmt.Errorf("server error: %s", string(b))
	}
	return string(b), nil
}

// makeConfirmURL returns an encoded URL used to confirm user's email.
func makeConfirmURL(opt *SignupOptions, cfg *config.Config, code string) (string, error) {
	hash, vals := signupRequestHash(cfg.UserName, cfg.Factotum.PublicKey)
	sig, err := cfg.Factotum.Sign(hash)
	if err != nil {
		return "", err
	}
	vals.Add("sigR", sig.R.String())
	vals.Add("sigS", sig.S.String())
	vals.Add("code", code)

	return (&url.URL{
		Scheme:   "http",
		Host:     opt.ServerEndpoint,
		Path:     "/confirm",
		RawQuery: vals.Encode(),
	}).String(), nil
}

// makeSignupURL returns an encoded URL used to sign up a new user.
func makeSignupURL(opt *SignupOptions, cfg *config.Config) (string, error) {
	hash, vals := signupRequestHash(cfg.UserName, cfg.Factotum.PublicKey)
	sig, err := cfg.Factotum.Sign(hash)
	if err != nil {
		return "", err
	}
	vals.Add("sigR", sig.R.String())
	vals.Add("sigS", sig.S.String())

	return (&url.URL{
		Scheme:   "http",
		Host:     opt.ServerEndpoint,
		Path:     "/signup",
		RawQuery: vals.Encode(),
	}).String(), nil
}

// signupRequestHash generates a hash of the supplied arguments
// that, when signed, is used to prove that a signup request originated
// from the user that owns the supplied private key.
func signupRequestHash(userName, publicKey string) ([]byte, url.Values) {
	const magic = "signup-request"
	u := url.Values{}
	h := sha256.New()
	h.Write([]byte(magic))
	w := func(key, val string) {
		var l [4]byte
		binary.BigEndian.PutUint32(l[:], uint32(len(val)))
		h.Write(l[:])
		h.Write([]byte(val))
		u.Add(key, val)
	}
	w("name", userName)
	w("key", publicKey)

	return h.Sum(nil), u
}

// writeConfigFile writes the given config to the configuration file.
func writeConfigFile(opt *SignupOptions, gopt *GlobalOptions, cfg *config.Config) error {
	if gopt.ConfigFile == "" {
		// The configuration file is not specified, use the default one.
		home, err := homedir.Dir()
		if err != nil {
			return err
		}
		gopt.ConfigFile = filepath.Join(home, config.DefBTVaultDir, config.DefConfigFile)
	}

	// Verify if we already have a configuration file.
	ok, err := utils.Exists(gopt.ConfigFile)
	if err != nil {
		return err
	}
	if ok && !opt.Force {
		return fmt.Errorf("%s already exists", gopt.ConfigFile)
	}

	// Create the configuration file.
	const create = os.O_RDWR | os.O_CREATE | os.O_TRUNC
	f, err := os.OpenFile(gopt.ConfigFile, create, 0640)
	if err != nil {
		// Directory doesn't exist, perhaps.
		if !os.IsNotExist(err) {
			return fmt.Errorf("cannot create %s: %v", gopt.ConfigFile, err)
		}
		dir := filepath.Dir(gopt.ConfigFile)
		if ok, _ := utils.Exists(dir); ok {
			// Looks like the directory exists, so stop now and report original error.
			return fmt.Errorf("cannot create %s: %v", gopt.ConfigFile, err)
		}
		if err = os.Mkdir(dir, 0700); err != nil {
			return fmt.Errorf("cannot make directory %s: %v", dir, err)
		}
		f, err = os.OpenFile(gopt.ConfigFile, create, 0640)
		if err != nil {
			return err
		}
	}
	defer f.Close()

	// Write the config.
	if _, err := f.WriteString(cfg.String()); err != nil {
		return err
	}

	fmt.Println("Configuration file written to:")
	fmt.Printf("\t%s\n\n", gopt.ConfigFile)

	return nil
}

// getUserInput shows the given prompt to the user and gets back
// an input line from him.
func getUserInput(pmt string) string {
	fmt.Print(pmt)
	s := bufio.NewScanner(os.Stdin)
	s.Scan()
	return s.Text()
}
