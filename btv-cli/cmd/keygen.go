package cmd

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"encoding/binary"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	"gitlab.fel.cvut.cz/mansumax/btvault/utils"

	homedir "github.com/mitchellh/go-homedir"

	"github.com/Bren2010/proquint"
	"github.com/spf13/cobra"
)

const (
	defCurveName = "p256"
	defKeyDir    = ".ssh"
	publicFile   = "public.btvkey"
	secretFile   = "secret.btvkey"
)

// KeygenOptions holds values for keygen flags.
type KeygenOptions struct {
	CurveName  string
	SecretSeed string
	KeyDir     string
	Force      bool
}

var keygenOptions KeygenOptions

// keygenCmd represents the keygen command
var keygenCmd = &cobra.Command{
	Use:   "keygen",
	Short: "Create a new key pair for the user",
	Long:  `The "keygen" command creates a new BTVault key pair and by default stores the pair in local files secret.btvkey and public.btvkey in $HOME/.ssh.  New users should instead use the signup command to create their first key. Keygen is usually used to restore keys from secret seeds. Secret seeds must be in proquint format.`,
	Run: func(cmd *cobra.Command, args []string) {
		if err := runKeygen(&keygenOptions, &globalOptions, args); err != nil {
			log.Fatalf("keygen: %v\n", err)
		}
	},
}

func init() {
	RootCmd.AddCommand(keygenCmd)

	fs := keygenCmd.Flags()

	fs.StringVar(&keygenOptions.CurveName, "curve", defCurveName, "cryptographic curve `name`: p256, p384 or p521")
	fs.StringVar(&keygenOptions.SecretSeed, "secretseed", "", "the `seed` containing a 128 bit secret in proquint format or a file that contains it")
	fs.StringVar(&keygenOptions.KeyDir, "keydir", "", "`directory` to store keys (default is $HOME/.ssh)")
	fs.BoolVarP(&keygenOptions.Force, "force", "f", false, "generate new keys even if older keys still exist")
}

// runKeygen is a main function for the keygen command.
func runKeygen(opt *KeygenOptions, gopt *GlobalOptions, args []string) error {
	if len(args) != 0 {
		return fmt.Errorf("too many arguments")
	}

	switch opt.CurveName {
	case "p256", "p384", "p521":
		// ok
	default:
		return fmt.Errorf("no such curve %q", opt.CurveName)
	}

	public, private, secretStr, err := createKeys(opt)
	if err != nil {
		return fmt.Errorf("creating keys: %v", err)
	}
	private = fmt.Sprintf("%s # %s\n", strings.TrimSpace(private), secretStr)

	// Verify keys directory
	if opt.KeyDir == "" {
		home, err := homedir.Dir()
		if err != nil {
			return err
		}
		opt.KeyDir = filepath.Join(home, defKeyDir)
	}
	absKeyDir, err := utils.AbsPathify(opt.KeyDir)
	if err != nil {
		return fmt.Errorf("%s: %v", opt.KeyDir, err)
	}
	opt.KeyDir = absKeyDir

	if err = writeKeys(opt, public, private); err != nil {
		return fmt.Errorf("writing keys: %v", err)
	}
	fmt.Println("BTVault private/public key pair written to:")
	fmt.Printf("\t%s\n", filepath.Join(opt.KeyDir, publicFile))
	fmt.Printf("\t%s\n", filepath.Join(opt.KeyDir, secretFile))
	fmt.Println("This key pair provides access to your BTVault identity and data.")
	if opt.SecretSeed == "" {
		fmt.Println("If you lose the keys you can re-create them by running this command:")
		fmt.Printf("\tbtv-cli keygen --secretseed %s\n", secretStr)
		fmt.Println("Write this command down and store it in a secure, private place.")
		fmt.Println("Do not share your private key or this command with anyone.")
	}
	fmt.Println()

	return nil
}

// writeKeys save both the public and private keys to their respective files.
func writeKeys(opt *KeygenOptions, publicKey, privateKey string) error {
	if _, err := os.Stat(opt.KeyDir); err != nil {
		if !os.IsNotExist(err) {
			return err
		}
		if err = os.MkdirAll(opt.KeyDir, 0700); err != nil {
			return err
		}
	}

	err := writeKeyFile(filepath.Join(opt.KeyDir, secretFile), privateKey, opt.Force)
	if err != nil {
		return err
	}
	err = writeKeyFile(filepath.Join(opt.KeyDir, publicFile), publicKey, opt.Force)
	if err != nil {
		return err
	}
	return nil
}

// writeKeyFile writes a single key to its file, removing the file
// beforehand if necessary due to permission errors.
func writeKeyFile(name, key string, force bool) error {
	// Verify if key file already exists.
	if ok, err := utils.Exists(name); err == nil && ok && !force {
		return fmt.Errorf("%s already exists", name)
	}
	const create = os.O_RDWR | os.O_CREATE | os.O_TRUNC
	fd, err := os.OpenFile(name, create, 0400)
	if os.IsPermission(err) && os.Remove(name) == nil {
		// Create may fail if file already exists and is unwritable,
		// which is how it was created.
		fd, err = os.OpenFile(name, create, 0400)
	}
	if err != nil {
		return err
	}
	defer fd.Close()
	_, err = fd.WriteString(key)
	return err
}

// createKeys creates a key pair based on the chosen curve and a secret seed.
func createKeys(opt *KeygenOptions) (public, private, secretStr string, err error) {
	// Pick secret 128 bits.
	var entropy []byte

	if opt.SecretSeed == "" {
		// No SecretSeed was given. Create a new secret seed.
		entropy = make([]byte, 16)
		if err = genEntropy(entropy); err != nil {
			return "", "", "", err
		}
		secretStr = proquint.Encode(entropy)
	} else {
		// Use provided SecretSeed
		ok, err := proquint.IsProquint(opt.SecretSeed)
		if err != nil {
			return "", "", "", err
		}
		if !ok {
			err = fmt.Errorf("invalid secret seed %s", opt.SecretSeed)
			return "", "", "", err
		}
		secretStr = opt.SecretSeed
		entropy = proquint.Decode(secretStr)
	}
	public, private, err = createKeysFromEntropy(opt, entropy)
	if err != nil {
		return "", "", "", err
	}

	return public, private, secretStr, nil
}

// drng is an io.Reader returning deterministic random bits seeded from aesKey.
type drng struct {
	aes     cipher.Block
	counter uint32
	random  []byte
}

func (d *drng) Read(p []byte) (n int, err error) {
	lenp := len(p)
	n = lenp
	var drand [16]byte
	for n > 0 {
		if len(d.random) == 0 {
			binary.BigEndian.PutUint32(drand[0:4], d.counter)
			d.counter++
			binary.BigEndian.PutUint32(drand[4:8], d.counter)
			d.counter++
			binary.BigEndian.PutUint32(drand[8:12], d.counter)
			d.counter++
			binary.BigEndian.PutUint32(drand[12:16], d.counter)
			d.counter++
			d.random = drand[:]
			d.aes.Encrypt(d.random, d.random)
		}
		m := copy(p, d.random)
		n -= m
		p = p[m:]
		d.random = d.random[m:]
	}
	return lenp, nil
}

// createKeysFromEntropy creates a key pair based on the chosen curve and a slice of entropy.
func createKeysFromEntropy(opt *KeygenOptions, entropy []byte) (public, private string, err error) {
	var curve elliptic.Curve
	switch opt.CurveName {
	case "p256":
		curve = elliptic.P256()
	case "p384":
		curve = elliptic.P384()
	case "p521":
		curve = elliptic.P521()
	default:
		err = fmt.Errorf("no such curve %q", opt.CurveName)
		return "", "", err
	}

	// Create crypto deterministic random generator from entropy.
	d := &drng{}
	cipher, err := aes.NewCipher(entropy)
	if err != nil {
		return "", "", err
	}
	d.aes = cipher

	// Generate random key-pair.
	priv, err := ecdsa.GenerateKey(curve, d)
	if err != nil {
		return "", "", err
	}

	// Encode keys.
	private = priv.D.String() + "\n"
	public = fmt.Sprintf("%s\n%s\n%s\n", opt.CurveName, priv.X.String(), priv.Y.String())

	return
}

// genEntropy fills the slice with cryptographically-secure random bytes.
func genEntropy(entropy []byte) error {
	_, err := rand.Read(entropy)
	if err != nil {
		return err
	}
	return nil
}
