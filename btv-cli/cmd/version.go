package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

const (
	logo = `
    ____ _______    __            ____ 
   / __ )_  __/ |  / /___ ___  __/ / /_
  / __  |/ /  | | / / __ ˇ/ / / / / __/
 / /_/ // /   | |/ / /_/ / /_/ / / /_  
/_____//_/    |___/\__,_/\__,_/_/\__/  
`
	version = "v0.0.1 (prototype)"
)

// versionCmd represents the version command
var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Show version",
	Long:  `The "version" command shows a BTVault's logo and the version of the client.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(logo)
		fmt.Println("version:", version)
	},
}

func init() {
	RootCmd.AddCommand(versionCmd)
}
