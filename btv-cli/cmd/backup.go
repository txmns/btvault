package cmd

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net/rpc"
	"os"
	"path/filepath"
	"strings"

	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/node"
	"gitlab.fel.cvut.cz/mansumax/btvault/config"
	"gitlab.fel.cvut.cz/mansumax/btvault/filter"
	"gitlab.fel.cvut.cz/mansumax/btvault/utils"

	"code.cloudfoundry.org/bytefmt"
	"github.com/spf13/cobra"
)

// BackupOptions holds values for backup flags.
type BackupOptions struct {
	Excludes    []string
	ExcludeFile string
	FilesFrom   string
}

var backupOptions BackupOptions

// backupCmd represents the backup command
var backupCmd = &cobra.Command{
	Use:   "backup [<path>...]",
	Short: "Create a new backup of files and/or directories",
	Long:  `The "backup" command starts the backup process and saves the files and directories given as the arguments.`,
	Run: func(cmd *cobra.Command, args []string) {
		if err := runBackup(&backupOptions, &globalOptions, args); err != nil {
			log.Fatalln(err)
		}
	},
}

func init() {
	RootCmd.AddCommand(backupCmd)

	fs := backupCmd.Flags()

	fs.StringSliceVarP(&backupOptions.Excludes, "exclude", "e", nil, "exclude a `pattern` (can be specified multiple times)")
	fs.StringVar(&backupOptions.ExcludeFile, "exclude-file", "", "read exclude patterns from a `file`")
	fs.StringVar(&backupOptions.FilesFrom, "files-from", "", "read the files to backup from `file` (can be combined with file args)")
}

func runBackup(opt *BackupOptions, gopt *GlobalOptions, args []string) error {
	// Load user's config.
	cfg, err := loadConfig(gopt.ConfigFile)
	if err != nil {
		return fmt.Errorf("loading config: %v", err)
	}

	// Start spinner.
	spin.Start()
	defer spin.Stop()

	if opt.FilesFrom != "" {
		files, err := readLinesFromFile(opt.FilesFrom)
		if err != nil {
			return err
		}
		// Merge files from files-from into normal args so we can reuse the normal
		// args checks and have the ability to use both files-from and args at the
		// same time.
		args = append(args, files...)
	}

	if len(args) == 0 {
		return fmt.Errorf("wrong number of parameters")
	}

	// Prepare list of target files to backup.
	target := make([]string, 0, len(args))
	for _, d := range args {
		if a, err := utils.AbsPathify(d); err == nil {
			d = a
		}
		target = append(target, d)
	}

	// Remove non-existing files.
	target, err = filterExisting(target)
	if err != nil {
		return err
	}

	// Merge patterns from file and command line.
	if opt.ExcludeFile != "" {
		patterns, err := readLinesFromFile(opt.FilesFrom)
		if err != nil {
			return err
		}
		opt.Excludes = append(opt.Excludes, patterns...)
	}

	excludeFilter := func(path string) bool {
		matched, err := filter.MatchList(opt.Excludes, path)
		if err != nil {
			log.Printf("error for exclude pattern: %v", err)
		}
		return !matched
	}

	stat, err := scan(target, excludeFilter)
	if err != nil {
		return err
	}
	spin.Stop()

	fmt.Printf("Found %d directories and %d files [%s]\n", stat.DirNumber, stat.FileNumber, bytefmt.ByteSize(stat.Size))

	spin.Start()
	res, err := callNodeBackup(cfg, stat.Files)
	if err != nil {
		return err
	}
	spin.Stop()

	fmt.Printf("Sliced %d files into %d blocks and %d fragments\n", res.FileNumber, res.BlockNumber, res.FragmentNumber)
	fmt.Println("Fragment distribution process is started")

	return nil
}

func callNodeBackup(cfg *config.Config, paths []string) (*node.BackupResponse, error) {
	client, err := rpc.DialHTTP("tcp", cfg.NodeEndpoint)
	if err != nil {
		return nil, fmt.Errorf("dialing: %v", err)
	}
	req := node.BackupRequest{
		Paths: paths,
	}
	res := node.BackupResponse{}
	err = client.Call("NodeRPC.Backup", req, &res)
	if err != nil {
		return nil, fmt.Errorf("calling node backup: %v", err)
	}
	return &res, nil
}

type FilterFunc func(path string) bool
type ScanStat struct {
	Files      []string
	FileNumber uint
	DirNumber  uint
	Size       uint64
}

// scan traverses the given paths and returns a ScanStat filled with information about
// encountered files and directories. Uses the filter function to exclude matching
// items with matching paths. scan does not follow symbolic links.
func scan(paths []string, filter FilterFunc) (ScanStat, error) {
	var stat ScanStat

	wf := func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Printf("error for %v: %v\n", path, err)
			return nil
		}
		if info == nil {
			log.Printf("error for %v: FileInfo is nil\n", path)
			return nil
		}
		if !filter(path) {
			if info.IsDir() {
				return filepath.SkipDir
			}
			return nil
		}
		if info.IsDir() {
			stat.DirNumber++
		} else {
			if info.Mode().IsRegular() {
				stat.FileNumber++
				stat.Size += uint64(info.Size())
				stat.Files = append(stat.Files, path)
			}
		}

		return nil
	}

	for _, path := range paths {
		err := filepath.Walk(path, wf)
		if err != nil {
			return ScanStat{}, fmt.Errorf("filepath.Walk: %v", err)
		}
	}

	return stat, nil
}

// filterExisting returns a slice of all existing items, or an error if no
// items exist at all.
func filterExisting(items []string) (result []string, err error) {
	for _, item := range items {
		if ok, err := utils.Exists(item); err != nil || !ok {
			continue
		}
		result = append(result, item)
	}

	if len(result) == 0 {
		return nil, fmt.Errorf("all target directories/files do not exist")
	}

	return
}

// readLinesFromFile reads all lines from the given file and returns them as a string slice.
// The comment lines (e.g. lines that start with a `#`) and blank lines are ignored.
func readLinesFromFile(name string) ([]string, error) {
	if name == "" {
		return nil, nil
	}
	f, err := os.Open(name)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	return readLinesFrom(f)
}

// readLinesFrom reads all lines and returns them as a string slice.
// The comment lines (e.g. lines that start with a `#`) and blank lines are ignored.
func readLinesFrom(r io.Reader) ([]string, error) {
	lines := make([]string, 0, 6)
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		if line != "" && !strings.HasPrefix(line, "#") {
			lines = append(lines, line)
		}
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return lines, nil
}
