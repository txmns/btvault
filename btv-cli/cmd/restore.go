package cmd

import (
	"fmt"
	"log"
	"net/rpc"
	"os"

	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/node"
	"gitlab.fel.cvut.cz/mansumax/btvault/config"
	"gitlab.fel.cvut.cz/mansumax/btvault/utils"

	"github.com/spf13/cobra"
)

// RestoreOptions holds values for restore flags.
type RestoreOptions struct {
	IDsFrom string
	OutDir  string
	Stdin   bool
}

var restoreOptions RestoreOptions

// restoreCmd represents the restore command
var restoreCmd = &cobra.Command{
	Use:   "restore [<id>...]",
	Short: "Restore backups",
	Long:  `The "restore" command restores backups of the files. IDs should be in the format <file id>[#<version timestamp>].`,
	Run: func(cmd *cobra.Command, args []string) {
		if restoreOptions.IDsFrom != "" && restoreOptions.Stdin {
			log.Fatalln("flags --ids-from and --stdin cannot be combined")
		}
		if err := runRestore(&restoreOptions, &globalOptions, args); err != nil {
			log.Fatalln(err)
		}
	},
}

func init() {
	RootCmd.AddCommand(restoreCmd)

	fs := restoreCmd.Flags()

	fs.StringVar(&restoreOptions.IDsFrom, "ids-from", "", "read IDs of the files to restore from `file` (cannot be combined with --stdin)")
	fs.StringVarP(&restoreOptions.OutDir, "output", "o", "", "output `directory` (default $PWD)")
	fs.BoolVar(&restoreOptions.Stdin, "stdin", false, "read IDs of the files to restore from standard input (default $PWD)")
}

func runRestore(opt *RestoreOptions, gopt *GlobalOptions, args []string) error {
	// Load user's config.
	cfg, err := loadConfig(gopt.ConfigFile)
	if err != nil {
		return fmt.Errorf("loading config: %v", err)
	}

	// Start spinner.
	spin.Start()
	defer spin.Stop()

	if opt.IDsFrom != "" {
		ids, err := readLinesFromFile(opt.IDsFrom)
		if err != nil {
			return err
		}
		// Merge ids from ids-from into normal args so we can reuse the normal
		// args checks and have the ability to use both ids-from and args at the
		// same time.
		args = append(args, ids...)
	}

	if opt.Stdin {
		ids, err := readLinesFrom(os.Stdin)
		if err != nil {
			return err
		}
		// Merge ids from stdin into normal args so we can reuse the normal
		// args checks and have the ability to use both stdin and args at the
		// same time.
		args = append(args, ids...)
	}

	if len(args) == 0 {
		return fmt.Errorf("wrong number of parameters")
	}

	// Prepare list of target ids to restore.
	target := make([]string, 0, len(args))
	target = append(target, args...)

	// Resolve output directory.
	if opt.OutDir == "" {
		dir, err := os.Getwd()
		if err != nil {
			return err
		}
		opt.OutDir = dir
	} else {
		absOutDir, err := utils.AbsPathify(opt.OutDir)
		if err != nil {
			return err
		}
		opt.OutDir = absOutDir
	}
	fi, err := os.Stat(opt.OutDir)
	if err != nil {
		return err
	}
	if !fi.IsDir() {
		return fmt.Errorf("%s not a directory", opt.OutDir)
	}

	res, err := callNodeRestore(cfg, opt.OutDir, target)
	if err != nil {
		return err
	}
	spin.Stop()

	for _, path := range res.Paths {
		fmt.Println(path)
	}

	return nil
}

func callNodeRestore(cfg *config.Config, outDir string, ids []string) (*node.RestoreResponse, error) {
	client, err := rpc.DialHTTP("tcp", cfg.NodeEndpoint)
	if err != nil {
		return nil, fmt.Errorf("dialing: %v", err)
	}
	req := node.RestoreRequest{
		OutDir: outDir,
		IDs:    ids,
	}
	res := node.RestoreResponse{
		Paths: make([]string, 0, len(ids)),
	}
	err = client.Call("NodeRPC.Restore", req, &res)
	if err != nil {
		return nil, fmt.Errorf("calling node restore: %v", err)
	}
	return &res, nil
}
